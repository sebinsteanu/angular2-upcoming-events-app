import{Component} from '@angular/core'
import { AuthService } from '../user/auth.service';
import { EventService, ISession } from '../events/index';


@Component({
    selector:'nav-bar',
    templateUrl:'app/nav/navbar.component.html',
    styles:[`
        .nav.navbar-nav {font-size: 16px;}
        #searchForm {margin-right: 77px;}
        @media (max-width: 1199px) {#searchForm {display:none}}
        li > a.active {color: #f97933; }
        `
    ]
})
export class NavBarComponent{
    searchTerm: string = ""
    foundSessions: ISession[]

    constructor(private auth:AuthService, private eventService:EventService) {
        
    }

    searchSessions(searchTerm:string){
        this.eventService.searchSessions(searchTerm).subscribe(sessions => {
                this.foundSessions = sessions
            }
        )
    }
}