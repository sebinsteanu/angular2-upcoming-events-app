"use strict";
function restrictedWords(words) {
    return function (formCtrl) {
        if (!words)
            return null;
        var invalidWords = words.map(function (w) { return formCtrl.value.includes(w) ? w : null; })
            .filter(function (w) { return w != null; });
        return invalidWords && invalidWords.length > 0 ? { 'restrictedWords': invalidWords.join(', ') } : null;
    };
}
exports.restrictedWords = restrictedWords;
//# sourceMappingURL=restricted-words.validator.js.map