import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "./shared/index";


@Component({
    templateUrl: "app/events/create-event.component.html",
    styles:[`
        em {color: red; float:right;}
        .error {background-color: #E3C3C5;}
    `]
})
export class CreateEventComponent{
    isDirty:boolean = true
    constructor(private router:Router, private eventService: EventService) {    
        
    }
    cancel(){
        this.router.navigate(['/events']);
    }

    saveEvent(formValues){
        console.log(formValues)
        this.eventService.saveEvent(formValues).subscribe(event => {
            this.router.navigate(['/events'])
            this.isDirty = false
        })
    }
}