import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Validators, FormControl, FormGroup } from "@angular/forms";
import { ISession, restrictedWords } from "../shared/index";

@Component({
    selector: 'create-session',
    templateUrl: "app/events/event-details/create-session.component.html",
    
    styles:[`
        em {color: red; float:right;}
        .error input, .error select, .error textarea {background-color: #E3C3C5;}
    `]
})
export class CreateSessionComponent implements OnInit {

    @Output() saveNewSession = new EventEmitter()
    @Output() cancelAddSession = new EventEmitter()

    newSessionForm: FormGroup
    name: FormControl
    duration: FormControl
    presenter: FormControl
    abstract: FormControl
    level: FormControl

    ngOnInit(){
        this.name = new FormControl('', Validators.required)
        this.presenter = new FormControl('', Validators.required)
        this.duration = new FormControl('', Validators.required)
        this.level = new FormControl('', Validators.required)
        this.abstract = new FormControl('', [Validators.required, Validators.maxLength(200), restrictedWords(['foo', 'bar'])])

        this.newSessionForm = new FormGroup({
            name: this.name,
            presenter: this.presenter,
            duration: this.duration,
            level: this.level,
            abstract: this.abstract
        })
    }


    saveSession(formValues){
        // console.log(formValues)

        let session:ISession = {
            id: undefined,
            name: formValues.name,
            presenter: formValues.presenter,
            duration: +formValues.duration,
            level: formValues.level,
            abstract: formValues.abstract,
            voters: []
        }
        this.saveNewSession.emit(session)
    }

    cancel(){
        this.cancelAddSession.emit()
    }
}