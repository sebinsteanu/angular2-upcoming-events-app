import { Component, OnInit } from "@angular/core";
import { EventService } from "../shared/events.service";
import { ActivatedRoute, Params } from "@angular/router";
import { IEvent, ISession } from "..";


@Component({
    templateUrl: 'app/events/event-details/event-details.component.html',
    styles:[`
        .container {padding: 20px; }
        .event-image {height: 120px;}
        a {cursor:pointer}
    `]
})
export class EventDetailsComponent implements OnInit{
    event:IEvent
    addMode:boolean
    filterBy:string = 'all'
    sortBy:string = 'votes'

    constructor(private eventService:EventService, private route:ActivatedRoute) {
        
    }
    ngOnInit(){
        this.route.data.forEach(data => {
            this.event = data['event']
            this.addMode = false                
        })

        // this.event = this.eventService.getEvent(+this.route.snapshot.params['id']);
    }

    addSession(){
        this.addMode = true
    }

    saveNewSession(session:ISession){
        const next = Math.max.apply(null, this.event.sessions.map(e => e.id))
        session.id = next + 1
        this.event.sessions.push(session)
        this.eventService.saveEvent(this.event).subscribe()
        this.addMode = false
    }

    cancelAddSession(){
        this.addMode = false
    }
}