import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'

import { EventsAppComponent} from './events-app.component'
import {
    EventsListComponent,
    EventThumbnailComponent,
    EventService,
    EventDetailsComponent,
    CreateEventComponent,
    EventListResolver, 
    CreateSessionComponent,
    SessionsListComponent,
    DurationPipe,
    UpvoteComponent,
    VoterService,
    LocationValidator,
    EventResolver} from './events/index'

import { NavBarComponent } from './nav/navbar.component'
import { JQ_TOKEN, TOASTR_TOKEN, IToastr, CollapsibleWell, SimpleModalComponent, ModalTrigger } from './common/index'
import { appRoutes } from './routes'
import { Error404Component } from './errors/404.component'
import { AuthService } from './user/auth.service'


declare let toastr: IToastr
declare let jQuery: Object

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    declarations: [
        EventsAppComponent,
        EventsListComponent,
        EventThumbnailComponent,
        NavBarComponent,
        EventDetailsComponent,
        CreateEventComponent,
        Error404Component,
        CreateSessionComponent,
        SessionsListComponent,
        CollapsibleWell,
        DurationPipe,
        SimpleModalComponent,
        ModalTrigger,
        UpvoteComponent,
        LocationValidator
    ],
    providers: [
        EventService,
        {
            provide: TOASTR_TOKEN,
            useValue: toastr
        },
        EventResolver,
        EventListResolver,
        {
            provide: 'CanDeactivateCreateEvent', 
            useValue: checkDirtyState
        },
        AuthService,
        {
            provide: JQ_TOKEN,
            useValue: jQuery
        },
        VoterService
    ],
    bootstrap: [EventsAppComponent]
})
export class AppModule{}

function checkDirtyState(component:CreateEventComponent) {
    if(component.isDirty)
        return window.confirm("You haven't saved your event yet! Are you sure you want to leave this page?");
    return true;
}