import { Component, OnInit, Inject } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { IToastr, TOASTR_TOKEN } from '../common/toastr.service';

@Component({
  templateUrl: 'app/user/profile.component.html',
  styles:[`
    em {color: red; float:right;}
    .error {background-color: #E3C3C5;}
  `]
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup

  private firstName: FormControl
  private lastName: FormControl

  constructor(private authService: AuthService, 
    private router:Router, 
    @Inject(TOASTR_TOKEN) private tostr: IToastr){
  }

  ngOnInit(): void {
    this.firstName = new FormControl(
      this.authService.currentUser.firstName, [Validators.required, Validators.pattern("[a-zA-Z]+")])
    this.lastName = new FormControl(
      this.authService.currentUser.lastName, Validators.required)
    this.profileForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName
    })
  }

  saveProfile(formValues){
    if(this.profileForm.valid){
      this.authService.updateCurrentUser(formValues.firstName, formValues.lastName).subscribe(() => 
      {
        this.tostr.success('Profile saved')
      })
    }
  }

  logout(){
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/user/login'])

    })
  }

  validateLastName(){
    return this.lastName.valid
  }
  
  validateFirstName(){
    return this.firstName.valid
  }

  cancel(){
    this.router.navigate(['events'])
  }
}