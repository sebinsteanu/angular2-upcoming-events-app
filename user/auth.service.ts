import { Injectable } from "@angular/core";
import { IUser } from "./user.model";
import { Http, RequestOptions, Headers } from "@angular/http";
import { Observable } from "rxjs/Rx";


@Injectable()
export class AuthService{
    currentUser:IUser

    constructor(private httpService: Http) {}

    loginUser(username: string, password: string){
        
        let headers = new Headers({'Content-Type': 'application/json'})
        let options = new RequestOptions({headers: headers})
        let loginInfo = {username: username, password: password}

        return this.httpService.post('/api/login', JSON.stringify(loginInfo), options).do( response => {
            if(response){
                this.currentUser = <IUser> response.json().user
            }
        }).catch(error => {
            return Observable.of(false);
        })
    }

    logout(){
        this.currentUser = undefined

        let headers = new Headers({'Content-Type': 'application/json'})
        let options = new RequestOptions({headers: headers})

        return this.httpService.post(`/api/logout`, JSON.stringify({}), options)
    }

    isAuthenticated():boolean{
        return !!this.currentUser;
    }

    checkAuthenticationStatus(){
        return this.httpService.get('/api/currentIdentity').map((response: any) => {
            if(response._body){
                return response.json();
            } else {
                return {};
            }
        }).do(currentUser => {
            if(!!currentUser.userName){
                this.currentUser = currentUser
            }
        }).subscribe()
    }
    
    updateCurrentUser(firstName: string, lastName: string){
        this.currentUser.firstName = firstName
        this.currentUser.lastName = lastName

        
        let headers = new Headers({'Content-Type': 'application/json'})
        let options = new RequestOptions({headers: headers})
        return this.httpService.put(`/api/users/${this.currentUser.id}`, JSON.stringify(this.currentUser), options)
    }
}