import { Component, style } from "@angular/core";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";

@Component({
    templateUrl:'app/user/login.component.html',
    styles:[`
        em { float:right; color:red;}
    `]
})
export class LoginComponent{
    loginInvalid:boolean = false

    constructor(private authService:AuthService, private router:Router){

    }

    login(formValues){
        // console.log(loginCredentials);
        this.authService.loginUser(formValues.userName, formValues.password).subscribe(response => {
            if(!response){
                this.loginInvalid = true
            } else{
                this.router.navigate(['events'])
            }
        })
    }
    
    cancel(){
        this.router.navigate(['events'])
    }

}